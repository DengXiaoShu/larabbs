<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\TopicRequest;
use App\Models\Topic;
use App\Models\User;
use App\Transformers\TopicTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TopicsController extends Controller
{
    public function store(TopicRequest $request, Topic $topic){
        $topic->fill($request->all());
        $topic->user_id = Auth::id();
        $topic->save();

        return $this->response->item($topic, new TopicTransformer())->setStatusCode(201);
    }

    public function update(TopicRequest $request,Topic $topic){
        $this->authorize('update', $topic);
        $topic->update($request->all());

        return $this->response->item($topic, new TopicTransformer());
    }

    public function destroy(Topic $topic){
        $this->authorize('destroy', $topic);
        $topic->delete();

        return $this->response->noContent();
    }

    public function index(Request $request, Topic $topic){
        $query = $topic->query();
        if($request->category_id){
            $query->where('category_id',$request->category_id);
        }
        switch ($request->order){
            //最新发布
            case 'recent':
                $query->recent();
                break;
            default:
                $query->recentReplied();
                break;
        }
        $topics = $query->paginate(5);

        return $this->response->paginator($topics, new TopicTransformer());
    }

    public function userIndex(User $user){
        $topics = $user->topics()->paginate(3);
        return $this->response->paginator($topics, new TopicTransformer());
    }

    public function show(Topic $topic){
        return $this->response->item($topic, new TopicTransformer());
    }
}
