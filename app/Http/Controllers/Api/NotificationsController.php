<?php

namespace App\Http\Controllers\Api;

use App\Transformers\NotificationTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function index(){
        $notifications = Auth::user()->notifications()->paginate(1);

        return $this->response->paginator($notifications, new NotificationTransformer());
    }

    public function stats(){
        return $this->response->array(['unread_count'=>1]);
    }
}
