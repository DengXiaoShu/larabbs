<?php

namespace App\Http\Middleware;

use Closure;

class EnsureEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() && !$request->user()->hasVerifiedEmail() && !$request->is('logout','email/*')){
            return  $request->expectsJson()?abort(403, 'Your email address is not verified.'): redirect()->route('verification.notice');;
        }
//        dd($request->is('logout','email/*'));
        return $next($request);
    }
}
